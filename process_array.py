#this script will take the images in p_data_jpeg
#read and normalize (described below) the images and create numpy arrays
#to represent input and output so we can train a NN

#the normalization procedure will be to place
#images that are too small on a background of 0 values
#the images will be centered and the resolution of all
#images will match the largest resolution in the set

in_prefix = "process/p_data_jpeg/input/"
label_prefix = "process/p_data_jpeg/labels/"

#grab max width and height and number of examples
with open("process/p_data_jpeg/metrics.txt") as f:
    f = f.read().split("\n")
    num_examples = int(f[0])
    width, height = map(int, f[2].split(","))

from PIL import Image
import numpy as np

#go through the dataset, for each image center the image
#on a background of 0's, then append them to a list (which will become arrays)

in_list, label_list = [], []

for i in range(num_examples):
    in_img = Image.open(in_prefix+"{}.jpeg".format(i))
    label_img = Image.open(label_prefix+"{}.jpeg".format(i))
    in_img_w, in_img_h = in_img.size
    label_img_w, label_img_h = label_img.size

    in_img_back = Image.new('RGB', (width, height), (0, 0, 0))
    label_img_back = Image.new('L', (width, height), 0)

    in_off = (width-in_img_w)//2, (height-in_img_h)//2
    label_off = (width-label_img_w)//2, (height-label_img_h)//2

    in_img_back.paste(in_img, in_off)
    label_img_back.paste(label_img, label_off)

    in_list.append(np.array(in_img_back))
    label_list.append(np.array(label_img_back))

print(np.array(in_list).shape)
print(np.array(label_list).shape)
