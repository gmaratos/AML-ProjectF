#This script will generate the jpeg pairs 
#input image in rgb and output which are annotations
in_path = "process/p_data_jpeg/input"
label_path = "process/p_data_jpeg/labels"

#here is where you specify the the objects you want
categories = ["dog"]
#in the data set, and your image sets will be in 
#process/p_data_jpeg

from pycocotools.coco import COCO

#we will only use data from the training set
data_type = "train2017"
annFile = "raw_data/mscoco/annotations/instances_{}.json".format(data_type)
jpeg_source_prefix = "raw_data/mscoco/{}/".format(data_type)
in_prefix = "process/p_data_jpeg/input/"
label_prefix = "process/p_data_jpeg/labels/"


#get the image indexes
coco = COCO(annFile)
catIds = coco.getCatIds(catNms=categories);
imgIds = coco.getImgIds(catIds=catIds );
imgs = coco.loadImgs(imgIds)

from PIL import Image, ImageDraw
import numpy as np
from shutil import copyfile


def translate(obj, width, height):
#takes segmentation and translates from rle
#lets return coordinates for draw
    msk = np.zeros(width * height)
    pos = 0
    counts = obj["counts"]
    for i in range(0, len(counts)-1, 2):
        pos += counts[i]
        for j in range(counts[i+1]):
            pos += 1
            msk[pos] = 1
    msk = msk.reshape((height, width)) 
    return [tuple(x) for x in list(np.argwhere(msk>0))] #converts msk to list of tuple coordinates

#go through the images and build jpeg dataset
j = 0
sw, sh = 1e20, 1e20 #ugly hack
lw, lh = 0, 0
for image in imgs:
    annIds = coco.getAnnIds(imgIds=image['id'], catIds=catIds, iscrowd=None) #need to investigate iscrowd=None
    anns = coco.loadAnns(annIds)
    width = image["width"]
    height = image["height"]

    #collected for metrics.txt
    if sw > width:
        sw = width
    if sh > height:
        sh = height
    if lw < width:
        lw = width
    if lh < height:
        lh = height

    img = Image.new('L', (width, height), 0)

    for ann in anns: #the current layout will only fill 1's where ever there is a label
        if ann["iscrowd"]:
            coords = translate(ann["segmentation"], width, height)
            ImageDraw.Draw(img).point(coords, fill=1) 
        else:
            poly = ann["segmentation"][0]
            ImageDraw.Draw(img).polygon(poly, outline=1, fill=1) 

    #need to find jpeg original and create processed dataset
    copyfile(jpeg_source_prefix+"{}".format(image['file_name']), in_prefix+"{}.jpeg".format(j))
    img.save(label_prefix+"{}.jpeg".format(j))
    j += 1

f = open("process/p_data_jpeg/metrics.txt", "w")
f.write("{}\n".format(len(imgs)))
f.write("{}, {}\n".format(sw, sh))
f.write("{}, {}\n".format(lw, lh))
