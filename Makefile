mscoco:
	wget http://images.cocodataset.org/zips/train2017.zip --directory-prefix=raw_data/mscoco/
	wget http://images.cocodataset.org/annotations/annotations_trainval2017.zip --directory-prefix=raw_data/mscoco/
