\documentclass{article} % For LaTeX2e
\usepackage{nips15submit_e,times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
%\documentstyle[nips14submit_09,times,art10]{article} % For LaTeX 2.09


\title{Instance Segmentation with Mask RCNN}


\author{
George Maratos\\
Department of Computer Science\\
UIC\\
None\\
\texttt{gmarat2@uic.edu} \\
\And
Ashwani Khemani \\
Department of Computer Science \\
None \\
\texttt{akhema2@uic.edu} \\
}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\begin{document}


\maketitle

\begin{abstract}
The Mask R-CNN (Regional Convolutional Neural Network) can be used in 
instance segmentation, and has been quite sucessful when compared with 
other architectures for the same task. In this project we look at its
performance on two well known datasets, cityscapes and CSAIL Places, and
try to see the impact of modifying hyperparameters and using different
network architectures in the backbone.
\end{abstract}

\section{Summary of the Problem}
Instance Segmentation is the combination of three other tasks making it
very difficult. Those three tasks are Image Classification, Object Detection,
and Semantic Segmentation. See Figure 1 for an overview of the task.

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.8\linewidth]{Figures/task_overview.png}
\end{center}
\caption{Overview of Task}
\end{figure}

\subsection{Instance Segmentation}
Image Classification, being the most studied of 
the three, is a task where you are given an image and the goal is to 
classify it. Examples of such studies are those done on the MNIST dataset [1].
In MNIST, the task is to classify handwritten digits given only an image of the
digit. Some of the most successful approaches involve CNN architectures, like
VGG, GoogleLeNet, ResNet, and many others.

Object Detection has the same domain, but the goal is to find all 
instances of an object and find the smallest bounding box around the object.
One way to structure this as a machine learning task is to formulate it as
a Regression problem, where the bounding box is defined by a set of coordinates.
This is done in our Mask R-CNN implementation and its immediate predecessor, 
Faster R-CNN [2].

The goal of Semantic Segmentation is to classify each pixel, effectively
segmenting an image into its semantic components. This was a difficult task
because there was no obvious way to generate predictions for each pixel, without
feeding the whole image multiple times through a traditional CNN classification
network, and this would make the problem intractable. One well known solution to
this is to use a Fully Convolutional Network (FCN), which would have an 
Encoder/Decoder structure by first using Convolution Layers and Pooling Layers
followed by De-Convolutional Layers which would reverse the pooling process [3].

\subsection{Cityscapes}
Cityscapes has data from street scenes, so an application of a 
network trained on this dataset is autonomous vehicles.
The dataset has approximately 5,000 examples and 30 classes with fine grain
annotations. We used about 3,500 for training and 500 for validation.

\subsection{CSAIL Places}
This dataset is more general and has a wide variety of scenes.
It has approximately 20K examples (2K validation), with annotations,
and 100 classes.
The CSAIL places dataset
will give us an opportunity to compare our results with work published that
tested with more popular datasets like MSCOCO [4].

\section{Approach}
\subsection{Mask R-CNN}
Mask R-CNN was developed at Facebook Ai Research by He et al. [5]
We chose to use Mask R-CNN for the Instance Segmentation task because it is
relatively new (The most recent version of the publication is from Jan 2018) 
and it had very good performance when compared to other models in the same
task. For an overview of the architecture, please see Figure 2.

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.5\linewidth]{Figures/arch_mask.png}
\end{center}
\caption{Mask R-CNN Architecture}
\end{figure}

The image is passed as input into the Convolutional Backbone. The backbone
is a CNN that extracts a feature map from the input. There are many options
for backbone architectures, for ours we chose to use ResNet101 and ResNet50 
for our experiments. Our motivations for doing so will be described later.
Once the feature maps are extracted, those maps are passed as input to a
Region Proposal Network (RPN). The RPN's role is to generate Regions of 
Interest (ROI), which are given used as rough estimates for the locations
of objects in the image. 

The next step in the network is to refine the bounding boxes, classify the
objects, and predict the segment mask based of of the ROI's. It is required
that the ROI's be the same dimensions so before this step there is an ROI
Pooling Layer. One of the improvements of Mask R-CNN over its immediate 
predecessor is that it uses a process called ROI Alignment to preserve 
spatial information that existed in the original ROI's before they were
pooled so that they have the same dimension. The ROI alignment is useful
because when the spatial information is lost, the fine grained precision
of the predicted mask suffers along its boundary. 

Following the pooling, the output is passed to two branches that run in
parallel. These two branches are collectively called the head of the network.
The first branch has deep fully connected layers, and it outputs
a bounding box/class for each instance much like Faster R-CNN. The difference
between Mask R-CNN and its immediate predecessor, at this point in the
architecture is its second branch which outputs a binary mask for each 
instance. The bounding box is refined via a task known as bounding box
regression from the original ROI bounding box.
Predicting a binary mask as opposed to a classifying mask
is important, according to the authors,
because it decouples the segmentation from the classification task (unlike
other segmentation architectures) which leads to better performance.

\subsection{Backbone}
The particular backbones we use are Feature Pyramid Networks (FPN) with either
a ResNet101 or ResNet50 as its Bottom Up Architecture.
An FPN first extracts features from its input using a Bottom Up 
Architecture, with each level extracting features with greater semantic 
value. Then using a Top Down Architecture and lateral connections from
the Bottum Up Architecture we can include features exracted in lower levels,
which will contain important features lost in the higher levels. Using an
FPN will enable the network to extract features from different levels of scale
in the images. See Figure 3 for an overview of the FPN, taken from the paper
it was originally proposed [6].

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.5\linewidth]{Figures/fpn_arch.png}
\end{center}
\caption{Feature Pyramid Network Architecture}
\end{figure}

The ResNet networks [7] are a powerful and popular networks for feature
extraction from images. They are also useful because they introduce skip
connections within a residual block 
so that layers can copy their input to the next layer. It encourages
the next layer to learn something unique from what has already been encoded. This
is also known to have positive effect on the training process by helping to
prevent vanishing/exploding gradients. See Figure 4 for a generalized example
of the residual block [7].

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.3\linewidth]{Figures/res_block.png}
\end{center}
\caption{Residual Block}
\end{figure}

\subsection{Transfer Learning}
The datasets we have chosen are objectively too small to train the entire
Mask R-CNN and have good generalization, but it might be enough to train
the head of the network. Considering this we use a technique called transfer
learning to take weights from a network trained on a similar task and place them
in the backbone of our network.

We will use weights trained on the MSCOCO dataset. The intuition is that the
pretrained backbone will already be able to extract all the low to high level
features from the input image, and only the head of the network needs to learn
how to interpret this rich feature map. For this project, we chose to train just
10 epochs on the head of the network, which seemed to be sufficient for good
performance.
Training is done by Stochastic Gradient Descent with Momentum, with a learning
rate of 0.001. We did not have serious issues in the training process so we
saw no need to experiment with other learning algorithms.

\subsection{Measuring Loss}
We have chosen to measure loss in the same way as the authors of the original
paper, which is a summation of the loss of each output (Bounding Box, Class,
and Mask). A high level description of the loss metric can be seen in equation
1.
\begin{equation}
L = L_{cls} + \lambda[u \geq 1]L_{loc} + L_{mask}
\end{equation}
$L_{cls}$ is the class loss, which is measured as log loss. $L_{loc}$ is
the measured L1 loss between the predicted bounding box for class $u$ and
the ground truth bounding box. $L_{mask}$ is the mean binary cross entropy 
of the predicted mask (where each pixel is generated by a sigmoid) and the
ground truth mask. The RPN has the same loss metric, but there is no $L_{mask}$.

\subsection{Hardware}
Given the scale and complexity of Mask R-CNN, we chose to accelerate the
training process using a Graphical Processing Unit. We trained in two
environments, the first was provided by the ACM Sig SysAdmin which has
an NVIDIA Tesla K80 12G, and the second was AWS which has NVIDIA Tesla M60 8G.
All of our code was written in Python, Tensorflow, and Keras. We also took advantage of an existing framework for Mask R-CNN so that we can focus our efforts more on running different experiments with the model on different datasets . We wrote extensions to the framework so that we can run it on different datasets and wrote relevant code when we made core change to the architecture of the model . [8].
There was variation in training time, depending on the model being trained.
An epoch typically took 1200 seconds when just the heads are updated, and
double this if we trained the whole network.

\section{Evaluation Metrics }

\subsection{Intersection over union(IoU)}
Intersection over Union measures how much overlap exists between the predicted instance mask and the ground-truth instance mask. IoU tells us how good is our prediction in the object detector with the ground truth (the real object boundary) . Figure 5 shows the formula and visual representation of how IoU is calculated for a given object .

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.3\linewidth]{Figures/iou_equation.png}
\end{center}
\caption{Computing the Intersection of Union}
\end{figure}

\subsection{Average Precision (AP)}
Average Precision is used to measure the accuracy of object detectors like mask R-CNN, SSD, etc. It is the average of the maximum precisions at different recall values. For our results , the average precision has been evaluated using mask IoU . We calculate the AP for each image in the validation set and then report the mean AP for the set . 

\subsection{Precision and recall}

Precision measures how accurate are our predictions. i.e. the percentage of our positive predictions that are correct.
It is defined as the number of true positives $(T_p)$ over the number of true positives plus the number of false positives $(F_p)$.

Recall measures how well we can find all the positives.It is defined as the number of true positives $(T_p)$ over the number of true positives plus the number of false negatives $(F_n)$.

Figure 6 gives the formula used to compute the average precision, which involves using precision at different recall values .
$p_{interp}(r)$ is the maximum precision for any recall values exceeding $r$.

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.8\linewidth]{Figures/mAP.png}
\end{center}
\caption{Computing the Mean Average Precision}
\end{figure}

\subsection{Precision-recall curve}
The precision-recall curve represents the tradeoff between precision and recall for different thresholds. If the area under the curve is more, it refers to high recall and high precision, where high precision means a low false positive rate, and high recall means a low false negative rate. 
If we get high scores for both, we can conclude the classifier is returning accurate results (high precision), as well as returning a majority of all positive results (high recall).

A system with high recall but low precision returns many results, but many of its predicted labels are incorrect when compared to the training labels. Likewise, A system with high precision but low recall works in the opposite manner, returning very few results, but most of its predicted labels are correct when compared to the training labels. An ideal system is the one which has high precision and high recall.
Figure 7 shows a precision recall curve plotted for inference done on one of the validation set images using our trained model (Cityscape-ResNet-101-FPN) 

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.6\linewidth]{Figures/pr_curve.png}
\end{center}
\caption{Precision - Recall Curve }
\end{figure}

\section{Experiments: Instance Segmentation}

\subsection{Architecture - backbone}
The mask R-CNN model is made up of two major components the head part of the network and the backbone, as described earlier. 
The model was trained using two different backbones : ResNet-FPN-50 and ResNet-FPN-101, to evaluate the impact of increasing the depth of backbone. We saw that increasing the depth of the backbone led to increase in performance of the network.

\subsection{RoI Proposal}
ROI plays a major role in deciding what regions will be used by the network to make predictions for the different instances in an image.
The number of ROI's can be controlled using a set of hyperparameters : one is the RPN threshold , which is used by the Region Proposal Network to control the number of regions proposed and RoI/images which controls the number of regions per image fed to the mask part of the network. 
We tried increasing the threshold and ROI/image factor, it gave similar results and sometimes led to decrease in the performance of the model. 
This might be due to the network generating more negative examples when the Region of Interests are increased leading to more negative examples as compared to positive examples for instances. 

\subsection{Architecture - head }
	
Extension of head : 
We tried increasing the number of convolutional layers in the mask head of the network (from the original 4) which led to decrease in the Average Precision of our model. This result was in contradiction of our expectation. 

Concatenation of output : 
One of the techniques we used to improve the performance of the head branch of the network was to concatenate the output of the different convolutional layers 
and then use the concatenated output for further classification. The concatenation gave similar performance and sometimes led to increase in performance of the network.

\subsection{Intersection Over Union }
By increasing the threshold for IoU, we can evaluate the confidence of the network in its predictions. We tried using two IoU threshold 0.5 and 0.75 .

\subsection{Learning Techniques }
We tried different approaches to train our network. 

1) One approach is to train only the head part of the network and use transfer learning for the backbone.
We used pretrained MSCOCO model weights as initialization of our backbone. This helped us in significantly reducing our training time.

2) Another technique to train the network was to train the head of the network for a certain number of epochs and then train the entire network using the weights obtained from previous epochs. We observed that this learning technique led to increase in the overall performance of the network. For cityscape , we are able to get a mAP of 0.40 using this approach.

3) One more approach to train the network can be to train the entire network i.e both the head and backbone part of the network. We observed that the performance of the network improved by using this learning technique . We got an AP of 0.24 for Places dataset using this approach of learning .

\begin{table}[t]
\caption{Architecture - Backbone }
\begin{center}
\begin{tabular}{ |p{5cm}|p{3cm}|p{3cm}|  }
\hline 
\multicolumn{3}{|c|}{Cityscapes} \\
\hline 
$net-depth-feature(Backbone)$ & $AP_{50}$& $AP_{75}$ \\
\hline 
$ResNet-50-FPN$ &$0.33$ &$0.16$ \\
\hline 
$ResNet-101-FPN$ &$0.36$ &$0.19$  \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}[t]
\caption{ROI - propoals}
\begin{center}
\begin{tabular}{ |p{5cm}|p{3cm}|p{3cm}|  }
\hline 
\multicolumn{3}{|c|}{Cityscapes} \\
\hline 
$RPN threshold$ |  $ROI/image$ & $AP_{50}$& $AP_{75}$ \\
\hline 
$0.7 | 200 $ &$0.36$ &$0.19$ \\
\hline 
$0.9 | 400 $ &$0.33$ &$0.2$  \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}[t]
\caption{Architecture - head }
\begin{center}
\begin{tabular}{ |p{5cm}|p{3cm}|p{3cm}|  }
\hline 
\multicolumn{3}{|c|}{Cityscapes} \\
\hline 
$net-depth-feature(Head)$ & $AP_{50}$& $AP_{75}$ \\
\hline 
$FPN-with-extended-head $ &$0.27$ &$0.12$ \\
\hline 
$Head-with-concatenation$ &$0.36$ &$0.20$ \\

\hline
\end{tabular}
\end{center}
\end{table}


\begin{table}[t]
\caption{Architecture - Backbone }
\begin{center}
\begin{tabular}{ |p{5cm}|p{3cm}|p{3cm}|  }
\hline 
\multicolumn{3}{|c|}{Places} \\
\hline 
$net-depth-feature(Backbone)$ & $AP_{50}$& $AP_{75}$ \\
\hline 
$ResNet-50-FPN$ &$0.16$ &$0.04$ \\
\hline 
$ResNet-101-FPN$ &$0.20$ &$0.10$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}[t]
\caption{ROI - propoals }
\begin{center}
\begin{tabular}{ |p{5cm}|p{3cm}|p{3cm}|  }
\hline 
\multicolumn{3}{|c|}{Places} \\
\hline 
$RPN threshold$ |  $ROI/image$ & $AP_{50}$& $AP_{75}$ \\
\hline 
$0.7 | 200 $ &$0.20$ &$0.10$ \\
\hline 
$0.9 | 400 $ &$0.18$ &$0.11$  \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}[t]
\caption{Architecture - head }
\begin{center}
\begin{tabular}{ |p{5cm}|p{3cm}|p{3cm}|  }
\hline 
\multicolumn{3}{|c|}{Places} \\
\hline 
$net-depth-feature(Head)$ & $AP_{50}$& $AP_{75}$ \\
\hline 
$FPN-with-extended-head $ &$0.19$ &$0.10$ \\
\hline
$Head-with-concatenation$ &$0.22$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.6\linewidth]{Figures/Figure_1.png}
\end{center}
\caption{Cityscape validation set inference result for ResNet-101-FPN }
\end{figure}

\begin{figure}[h]
\begin{center}
%\framebox[4.0in]{$\;$}
%\fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
\includegraphics[width=0.9\linewidth]{Figures/Figure_3.png}
\end{center}
\caption{Chart showing the GT vs Prediction IoU values }
\end{figure}

\clearpage
\subsubsection*{References}
[1] LeCunn, Cortes, Burges. The MNIST Database of Handwritten Digits.
, {\it http://yann.lecun.com/exdb/mnist}.

[2] Ren, He, Girshick, Sun. (2016) Faster R-CNN: Towards Real-Time Object
Detection with Region Proposal. {\it CoRR}, arXiv:1506.01497

[3] Long, Shellhammer, Darrell. (2015) Fully Convolutional Networks 
for Semantic Segmentation. {\it CVPR2015}

[4] Lin, Maire, Belongie, et al. (2015)  Microsoft COCO: Common 
Objects in Context. {\it CoRR},	arXiv:1405.0312

[5] He, Gkioxari. Dollar, Girshick. (2018) Mask R-CNN. {\it CoRR},
arXiv:1703.06870

[6] Lin, Dollar, Girshick, et al. (2017) Feature Pyramid Networks 
for Object Detection, {\it CoRR}, arXiv:1612.03144

[7] He, Zhang, Ren, Sun. (2015) Deep Residual Learning for Image Recognition
{\it CoRR}, arXiv:1512.03385

[8] matterport. Mask R-CNN for object detection and instance 
segmentation on Keras and TensorFlow, {\it https://github.com/matterport/Mask\_RCNN}

\end{document}
